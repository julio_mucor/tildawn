package com.tildawn.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zeroone on 10/28/16.
 */
//Note the annotation is not “@RestController” it is just “@Controller” which is Spring MVC controller which returns a view.
@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        /*html is a static resource so you can access by "localhost:8080/index.html" or use "forward" keyword*/
        /*return "forward:index.html";*/
        return "index";
    }
}
