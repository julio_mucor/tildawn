package com.tildawn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TildawnApplication {

	public static void main(String[] args) {
		SpringApplication.run(TildawnApplication.class, args);
	}
}
